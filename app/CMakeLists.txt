
file(GLOB SOURCES_app ${CMAKE_SOURCE_DIR}/app/*.cpp)

add_executable(out ${SOURCES_app})
target_link_libraries(out containers)
#set_target_properties(out PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
