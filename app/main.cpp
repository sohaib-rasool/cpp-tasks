#include "containers.h"
#include"XYZ.h"
#include <iostream>
#include <vector>
using namespace std;

int main() {

  Containers obj;
  // display Hellow World
  obj.display() ;

  // VECTOR square function
  std::vector<int> Vector = {1, 3, 2, 7, 5, 7, 3, 4, 2, 9, 11, 4};
  std::vector<int> SqVector = obj.square(Vector);
  cout << "Vector Squared:" << endl;
  for (int i = 0; i < SqVector.size(); i++) {
    cout << Vector[i] << " " << SqVector[i] << endl;
  }

  // LIST square function
  std::list<int> List = {1, 3, 5, 7};
  std::list<int> SqList = obj.square(List);
  cout << "List Squared:" << endl;
  for (auto it = SqList.begin(); it != SqList.end(); ++it) {
    cout << *it << endl;
  }
  cout << "----------" << endl;

  // get and set sum Vector

  /*cout<<"Sum=0:"<<endl;
  cout<<obj.getSum()<<endl;
  cout<<"setSum"<<endl;
  obj.setSum(SqVector);
  cout<<"Sum>0:"<<endl;
  cout<<obj.getSum()<<endl;


  //get and set sum List

  cout<<"Sum=0:"<<endl;
  cout<<obj.getSum()<<endl;
  cout<<"setSum"<<endl;
  //obj.setSum(SqList);
  cout<<"Sum>0:"<<endl;
  cout<<obj.getSum()<<endl;
  */
  obj.setStat(Vector);
  cout << "Vector Stats:" << endl;
  cout << "SUM:" << obj.getSum() << endl;
  cout << "MAX:" << obj.getMax() << endl;
  cout << "MIN:" << obj.getMin() << endl;
  cout << "SIZE:" << obj.getSize() << endl;
  
  XYZ test_obj;
  


  return 0;
}
