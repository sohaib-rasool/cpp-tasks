#include "containers.h"

#include <algorithm>
#include <iostream>
//#include <list>
//#include <vector>
using namespace std;

void Containers::display()
{
   cout << "Hello World!" << endl;
}

std::vector<int> Containers::square(std::vector<int> input)
{

   for (int i = 0; i < input.size(); i++)
   {
      input[i] = input[i] * input[i];
   }

   return input;
}

std::list<int> Containers::square(std::list<int> input)
{

   for (auto it = input.begin(); it != input.end(); ++it)
   {
      *it = *it * *it;
   }

   return input;
}
Containers::Containers()
{
   sum_of_squares = 0;
  stat.size=50;
   stat.maximum        = 0;
   stat.minimum        = 0;
   stat.size           = 0;
   stat.sum_of_squares = 0;
}

Containers::~Containers()
{
  // cout << "destructor called" << endl;
}

int Containers::setStat(std::vector<int> input)
{

   std::vector<int> input_square = square(input);
   for (int i = 0; i < input_square.size(); i++)
   {

      stat.sum_of_squares += input_square[i];
   }
   stat.size = input_square.size();
   std::sort(input_square.begin(), input_square.end());
    
   // for_each(input_square.begin(), input_square.end(), printall);
   stat.maximum = input_square[input_square.size() - 1];
   stat.minimum = input_square[0];
   return 0;
}




int Containers::setStat(std::list<int> input)
{

   std::list<int> input_square = square(input);
   for (auto it = input_square.begin(); it != input_square.end(); ++it)
   {
      stat.sum_of_squares += *it;
   }
   input_square.sort();
   stat.size = input_square.size();
   //std::sort(input_square.begin(), input_square.end());

   // for_each(input_square.begin(), input_square.end(), printall);
   stat.maximum = *input_square.end();
   stat.minimum = *input_square.begin();
   
   return 0;
}

void Containers::printall(int x)
{
   cout << x << endl;
}

void Containers::setSum(std::list<int> input)
{

   for (auto it = input.begin(); it != input.end(); ++it)
   {
      sum_of_squares += *it;
   }
}

int Containers::getSum()
{
   return stat.sum_of_squares;
}
int Containers::getSize()
{
   return stat.size;
}
int Containers::getMax()
{
   return stat.maximum;
}
int Containers::getMin()
{
   return stat.minimum;
}
