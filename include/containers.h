#ifndef CONTAINERS_H
#define CONTAINERS_H

//#include <algorithm>
#include <list>
#include <vector>

class Containers {

private:
  int sum_of_squares;

  struct stats_squared {
    int sum_of_squares;
    int minimum;
    int maximum;
    int size;
  };
  stats_squared stat;

public:
  void display();
  Containers();
  ~Containers();

  std::vector<int> square(std::vector<int> input);
  std::list<int> square(std::list<int> input);
  void printall(int x);
  int setStat(std::vector<int> input);
  int setStat(std::list<int> input);
  void setSum(std::list<int> input);

  int getSize();
  int getMax();
  int getMin();
  int getSum();
};
#endif