#include "benchmark.h"
#include "containers.h"

#include <algorithm>

using namespace std;

Containers obj;

static void BM_Vector(benchmark::State &state)
{

   vector<int> example_vector(state.range(0));
   example_vector.reserve(state.range(0));
   generate(example_vector.begin(), example_vector.end(), rand);
   for (auto _ : state)
   {

      obj.setStat(example_vector);
   }
}

static void BM_LIST(benchmark::State &state)
{

   list<int> example_list(state.range(0));
   generate(example_list.begin(), example_list.end(), rand);
   for (auto _ : state)
   {

      obj.setStat(example_list);
   }
}

static void BM_Million(benchmark::State &state)
{
   for (auto _ : state)
   {
      for (int i = 0; i < 2000000; i++)
      {
         Containers test;
      }
   }
}
/*
static void BM_LIST_Million(benchmark::State &state)
{
   for (auto _ : state)
   {
      vector<int> example_list(4000000);
      generate(example_list.begin(), example_list.end(), rand);
      obj.setStat(example_list);
   }
}
*/
BENCHMARK(BM_Vector)->Arg(10000)->Arg(25000);
BENCHMARK(BM_LIST)->Arg(10000)->Arg(25000);
BENCHMARK(BM_Million);
// BENCHMARK(BM_LIST_Million);

BENCHMARK_MAIN();