#include "containers.h"

#include <gtest/gtest.h>
#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char **argv)
{

   testing::InitGoogleTest(&argc, argv);
   return RUN_ALL_TESTS();
}

// int main(){return 0;}

Containers       obj;
std::vector<int> Vector       = {1, 3, 2, 7, 5, 7, 3, 4, 2, 9, 11, 4};
std::vector<int> SquareVector = {1, 9, 4, 49, 25, 49, 9, 16, 4, 81, 121, 16};

std::list<int> List       = {1, 3, 2, 7, 5, 7, 3, 4, 2, 9, 11, 4};
std::list<int> SquareList = {1, 9, 4, 49, 25, 49, 9, 16, 4, 81, 121, 16};

TEST(containers, squareVector)
{
   ASSERT_EQ(SquareVector, obj.square(Vector));
}
TEST(containers, squareList)
{
   ASSERT_EQ(SquareList, obj.square(List));
}
TEST(containers, setStat)
{
   ASSERT_EQ(0, obj.setStat(Vector));
}
TEST(containers, getSum)
{
   ASSERT_EQ(384, obj.getSum());
}
TEST(containers, getSize)
{
   ASSERT_EQ(12, obj.getSize());
}
TEST(containers, getMax)
{
   ASSERT_EQ(121, obj.getMax());
}
TEST(containers, getMin)
{
   ASSERT_EQ(1, obj.getMin());
}
